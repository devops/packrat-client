SHELL = /bin/bash

.PHONY: build
build: src swagger.json swagger-codegen-v3
	docker run --rm -v "$(shell pwd):/data" swagger-codegen-v3 \
		generate -i ./swagger.json -l ruby -o ./src -c ./config.json

src:
	mkdir -p ./src

.PHONY: clean
clean:
	rm -f ./swagger.json
	rm -rf ./src/*

swagger.json:
	curl -sf https://packrat.oit.duke.edu/swagger.yaml | yq -o=json 1>./swagger.json

.PHONY: swagger-codegen-v3
swagger-codegen-v3:
	docker build -t swagger-codegen-v3 - < ./Dockerfile.swagger-codegen-cli

.PHONY: test
test:
	docker build -t packrat-client-test -f ./Dockerfile ./src
	docker run --rm packrat-client-test bundle exec rake
