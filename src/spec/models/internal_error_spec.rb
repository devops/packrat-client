=begin
#API V2

#The API for packrat. [Break out of the iframe here](/swagger/index.html)

OpenAPI spec version: v2

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.46
=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for Packrat::InternalError
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'InternalError' do
  before do
    # run before each test
    @instance = Packrat::InternalError.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of InternalError' do
    it 'should create an instance of InternalError' do
      expect(@instance).to be_instance_of(Packrat::InternalError)
    end
  end
  describe 'test attribute "errors"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
