# Packrat::RequestedState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**directory_spec** | **Array** | An array of JSON objects representing initial permissions and subdirectories to create (unstable API, documentation TBD) | [optional] 
**skip_perms** | **BOOLEAN** | Skip assigning the default user/group permissions on provision. Explicitly requested permissions will still be applied | [optional] 
**max_gb** | **Integer** | Total volume size (GB) | 
**share_nfs** | **BOOLEAN** | Share via NFS (either share_nfs or share_smb must be specified, but not both). Requires a nfs_export_spec be specified | [optional] 
**share_smb** | **BOOLEAN** | Share via NFS (either share_smb or share_nfs must be specified, but not both) | [optional] 
**nfs_export_spec** | [**Array&lt;RequestedStateNfsExportSpec&gt;**](RequestedStateNfsExportSpec.md) | Array of NFS exports (required for NFS share type) | [optional] 

