# Packrat::Volume

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** | Internal (SQL) ID | [optional] 
**type** | **String** | Volume type | [optional] 
**name** | **String** | Name | [optional] 
**fund_code** | **String** | Fund code | [optional] 
**fund_code_max_gb** | **Integer** | The maximum size to bill to the fund_code. remainder_fund_code will be billed for any allocation beyond fund_code_max_gb | [optional] 
**remainder_fund_code** | **String** | Code which will be billed for any allocation beyond fund_code_max_gb | [optional] 
**grouper_group_id** | **Integer** | Internal (SQL) ID of the grouper group | [optional] 
**location_id** | **Integer** | Internal (SQL) ID of the \&quot;location\&quot; | [optional] 
**active** | **BOOLEAN** | Confirmed to exist on the storage device? | [optional] [default to true]
**api_identifier** | **String** | The underlying storage device unique ID (for Isilon, the full path) | [optional] 
**annual_cost_per_gb** | **String** | This is the amount (in currency) charged per gigabyte of the total volume size (i.e. the max_gb) over a one year period. This includes any selected backup options. | [optional] 
**pricing** | **String** | The annual pricing for a volume. Divide by 12 for the current month. Includes backups. This calculation is expensive and this property is not shown on the index view | [optional] 
**max_gb** | **Integer** | The maximum size of a volume | [optional] 
**used_gb** | **Integer** | The currently utilized size of a volume | [optional] 
**decommissioned_at** | **String** | The ruby \&quot;to_s\&quot; representation of a decommission timestamp (for inactive volumes) | [optional] 
**decommissioned_size** | **Integer** | The maximum storage size (gb) at the time of decommission | [optional] 
**backups_on_site_retention** | **Integer** | The retention (in seconds) of Rubrik backups (first copy) | [optional] 
**backups_off_site_retention** | **Integer** | The retention (in seconds) of Rubrik backups (second copy) | [optional] 
**requested_backend_state** | **Object** | A JSON object representing the current backend state change request (if present) | [optional] 
**created_at** | **String** | The ruby \&quot;to_s\&quot; representation of creation time | [optional] 
**updated_at** | **String** | The ruby \&quot;to_s\&quot; representation of last update time | [optional] 
**mount_urls** | [**ComponentsschemasmountUrls**](ComponentsschemasmountUrls.md) |  | [optional] 
**url** | **String** | The API URL for this volume | [optional] 
**user** | **String** | The NetID of the owning user | [optional] 
**grouper_group** | **String** | The scoped grouper group stem | [optional] 

