# Packrat::RequestedStateNfsExportSpec

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** | An IP, IPv4 subnet (in CIDR notation), or a hostname which will be exported to | 
**type** | **String** | Type of export. \&quot;root\&quot; will have full control (no root squash). | 

