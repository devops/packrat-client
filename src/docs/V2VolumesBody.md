# Packrat::V2VolumesBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Volume name (alphanumeric and hyphens only, must be unique per location) | [optional] 
**location_id** | **Integer** | The ID of a location, as returned by the locations endpoint | 
**fund_code** | **String** | A fund code, hyphens optional | 
**fund_code_max_gb** | **Integer** | The maximum size to bill to the fund_code. If a remainder_fund_code is provided, it will be billed for any allocation beyond fund_code_max_gb | [optional] 
**remainder_fund_code** | **String** | If remainder_fund_code is provided, it will be billed for any allocation beyond fund_code_max_gb | [optional] 
**requested_state** | [**RequestedState**](RequestedState.md) |  | 

