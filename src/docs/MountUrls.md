# Packrat::MountUrls

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smb** | **Array&lt;String&gt;** |  | [optional] 
**nfs** | **Array&lt;String&gt;** |  | [optional] 

