# Packrat::VolumesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_v2_volumes_get**](VolumesApi.md#api_v2_volumes_get) | **GET** /api/v2/volumes | list volumes
[**api_v2_volumes_id_delete**](VolumesApi.md#api_v2_volumes_id_delete) | **DELETE** /api/v2/volumes/{id} | delete volume
[**api_v2_volumes_id_get**](VolumesApi.md#api_v2_volumes_id_get) | **GET** /api/v2/volumes/{id} | show volume
[**api_v2_volumes_id_put**](VolumesApi.md#api_v2_volumes_id_put) | **PUT** /api/v2/volumes/{id} | update volume
[**api_v2_volumes_post**](VolumesApi.md#api_v2_volumes_post) | **POST** /api/v2/volumes | create volume

# **api_v2_volumes_get**
> Array&lt;Componentsschemasvolume&gt; api_v2_volumes_get

list volumes

List volumes

### Example
```ruby
# load the gem
require 'packrat'
# setup authorization
Packrat.configure do |config|
end

api_instance = Packrat::VolumesApi.new

begin
  #list volumes
  result = api_instance.api_v2_volumes_get
  p result
rescue Packrat::ApiError => e
  puts "Exception when calling VolumesApi->api_v2_volumes_get: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;Componentsschemasvolume&gt;**](Componentsschemasvolume.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **api_v2_volumes_id_delete**
> api_v2_volumes_id_delete(id)

delete volume

Delete volumes

### Example
```ruby
# load the gem
require 'packrat'
# setup authorization
Packrat.configure do |config|
end

api_instance = Packrat::VolumesApi.new
id = 'id_example' # String | id


begin
  #delete volume
  api_instance.api_v2_volumes_id_delete(id)
rescue Packrat::ApiError => e
  puts "Exception when calling VolumesApi->api_v2_volumes_id_delete: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id | 

### Return type

nil (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **api_v2_volumes_id_get**
> Componentsschemasvolume api_v2_volumes_id_get(id)

show volume

Show volume

### Example
```ruby
# load the gem
require 'packrat'
# setup authorization
Packrat.configure do |config|
end

api_instance = Packrat::VolumesApi.new
id = 'id_example' # String | id


begin
  #show volume
  result = api_instance.api_v2_volumes_id_get(id)
  p result
rescue Packrat::ApiError => e
  puts "Exception when calling VolumesApi->api_v2_volumes_id_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id | 

### Return type

[**Componentsschemasvolume**](Componentsschemasvolume.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **api_v2_volumes_id_put**
> Componentsschemasvolume api_v2_volumes_id_put(id, opts)

update volume

Update volume

### Example
```ruby
# load the gem
require 'packrat'
# setup authorization
Packrat.configure do |config|
end

api_instance = Packrat::VolumesApi.new
id = 'id_example' # String | id
opts = { 
  body: Packrat::VolumesIdBody.new # VolumesIdBody | 
}

begin
  #update volume
  result = api_instance.api_v2_volumes_id_put(id, opts)
  p result
rescue Packrat::ApiError => e
  puts "Exception when calling VolumesApi->api_v2_volumes_id_put: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| id | 
 **body** | [**VolumesIdBody**](VolumesIdBody.md)|  | [optional] 

### Return type

[**Componentsschemasvolume**](Componentsschemasvolume.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **api_v2_volumes_post**
> Componentsschemasvolume api_v2_volumes_post(opts)

create volume

Create volumes

### Example
```ruby
# load the gem
require 'packrat'
# setup authorization
Packrat.configure do |config|
end

api_instance = Packrat::VolumesApi.new
opts = { 
  body: Packrat::V2VolumesBody.new # V2VolumesBody | 
}

begin
  #create volume
  result = api_instance.api_v2_volumes_post(opts)
  p result
rescue Packrat::ApiError => e
  puts "Exception when calling VolumesApi->api_v2_volumes_post: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**V2VolumesBody**](V2VolumesBody.md)|  | [optional] 

### Return type

[**Componentsschemasvolume**](Componentsschemasvolume.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



