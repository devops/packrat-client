# Packrat::Forbidden

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | **Array&lt;String&gt;** |  | [optional] 

