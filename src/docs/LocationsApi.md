# Packrat::LocationsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_v2_locations_get**](LocationsApi.md#api_v2_locations_get) | **GET** /api/v2/locations | list locations

# **api_v2_locations_get**
> Array&lt;InlineResponse200&gt; api_v2_locations_get

list locations

### Example
```ruby
# load the gem
require 'packrat'
# setup authorization
Packrat.configure do |config|
end

api_instance = Packrat::LocationsApi.new

begin
  #list locations
  result = api_instance.api_v2_locations_get
  p result
rescue Packrat::ApiError => e
  puts "Exception when calling LocationsApi->api_v2_locations_get: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Array&lt;InlineResponse200&gt;**](InlineResponse200.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



