# Packrat::InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** | Location ID (used when requesting a new volume) | [optional] 
**fqdn** | **String** | Primary DNS FQDN of the location | [optional] 
**name** | **String** | The human legible name, shown in the browser UI | [optional] 
**min_gb** | **Integer** | Minimum volume size (GB) | [optional] 
**max_gb** | **Integer** | Maximum volume size (GB) | [optional] 
**secure** | **BOOLEAN** | Suitable for \&quot;secure\&quot; data | [optional] 
**restricted** | **BOOLEAN** | Limited to specific user/group membership | [optional] 
**active** | **BOOLEAN** | Currently active/enabled | [optional] 
**system** | **BOOLEAN** | Reserved for internal system use | [optional] 
**backups_on_site_retention** | **Integer** | Time (seconds) to set for new rubrik backup retentions | [optional] 
**backups_off_site_retention** | **Integer** | Time (seconds) to set for new rubrik off-site backup retentions | [optional] 
**snapshot_retention_days** | **Integer** | Snapshot retention days | [optional] 
**backup_type** | **String** | Type of backups to configure on new volumes | [optional] 
**cnames** | **Array&lt;String&gt;** | CNAMEs | [optional] 

